CREATE TABLE whiskies
(
	Id int PRIMARY KEY IDENTITY,
	Brand varchar (44) NOT NULL,
	Product varchar (44) NOT NULL,
	Year int NOT NULL,
	Description varchar (1000),
	Price double NOT NULL
);

CREATE TABLE USER
(
	id INT PRIMARY KEY IDENTITY, 
	firstName VARCHAR(32) NOT NULL,
	lastName VARCHAR(32) NOT NULL,
	addressLine1 VARCHAR(32) NOT NULL,
	addressLine2 VARCHAR(32),
	town VARCHAR(32) NOT NULL,
	county VARCHAR(32) NOT NULL,
	userName VARCHAR(32) NOT NULL,
	password VARCHAR(32) NOT NULL
) ;
