package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import model.Whiskey;

public enum WhiskeyDAO {

	INSTANCE;

	public Connection getConnection() {
		Connection con = null;
		try {
			Class.forName("org.hsqldb.jdbcDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/oneDB", "sa", "");
		} catch (Exception e) {

			e.printStackTrace();
		}
		return con;
	}
	
	public void save(Whiskey whiskey) {

		try {
			Connection con = getConnection();
			Statement stmt = con.createStatement();

			stmt.executeUpdate("INSERT INTO Whiskies (brand, product, year, description, price)" + "VALUES ('"
					+ whiskey.getBrand() + "','" + whiskey.getProduct() + "','" + whiskey.getYear() + "','" + whiskey.getDescription() + "','" + whiskey.getPrice() + "')");
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
	
	public void search(Whiskey whiskey){
		
		try{
			Connection con = getConnection();
			Statement stmt = con.createStatement();
			
			stmt.executeQuery("SELECT * FROM whiskies where brand like ?");
			//stmt.executeQuery("SELECT * FROM whiskies where brand like '%'");
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
}
