package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.User;

public enum CheckLoginDAO {
	
	INSTANCE;

	public Connection getConnection() {
		
		Connection con = null;
		try {
			Class.forName("org.hsqldb.jdbcDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/oneDB", "sa", "");
		} catch (Exception e) {

			e.printStackTrace();
		}
		return con;
	}

	
}
