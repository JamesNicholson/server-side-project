package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.User;

public enum UserDAO {
	
	
	INSTANCE;

	public Connection getConnection() {
		Connection con = null;
		try {
			Class.forName("org.hsqldb.jdbcDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/oneDB", "sa", "");
		} catch (Exception e) {

			e.printStackTrace();
		}
		return con;
	}

	public void save(User user){
		Connection connection = getConnection();
		
		try{
			PreparedStatement psmt = connection.prepareStatement("INSERT INTO user (firstName, lastName, addressLine1, addressLine2, town, county, username, password) VALUES (?, ?, ?, ?, ?, ? ,?, ?)");
			psmt.setString(1, user.getFirstName());
			psmt.setString(2, user.getLastName());
			psmt.setString(3, user.getAddressLine1());
			psmt.setString(4, user.getAddressLine2());
			psmt.setString(5, user.getTown());
			psmt.setString(6, user.getCounty());
			psmt.setString(7, user.getUserName());
			psmt.setString(8, user.getPassword());
			
			psmt.executeUpdate();
			psmt.close();
			connection.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public User checkLogin(String userName, String password){
		Connection connection = getConnection();
		User user = null;
		
		try{
			PreparedStatement psmt = connection.prepareStatement("SELECT * FROM user WHERE userName = ? AND password = ?");
			psmt.setString(1, userName);
			psmt.setString(2, password);
			ResultSet rs = psmt.executeQuery();
			
			if(rs.next()){
				user = new User(
						rs.getString("firstName"),
						rs.getString("lastName"),
						rs.getString("addressLine1"),
						rs.getString("addressLine2"),
						rs.getString("town"),
						rs.getString("county"),
						rs.getString("userName"),
						rs.getString("password"));
			}
			psmt.close();
			connection.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return user;
	}
}
