package controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SearchWhiskeyServlet
 */
@WebServlet("/SearchWhiskeyServlet")
public class SearchWhiskeyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchWhiskeyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		//int id = Integer.parseInt(request.getParameter("id"));
		String brand = request.getParameter("searchBrand");
		
		
		request.getRequestDispatcher("index.jsp").forward(request, response);
		
		
		System.out.println(brand);
		//String product = request.getParameter("product");
		//String year = request.getParameter("year");
		//String description = request.getParameter("description");
		//double price = Double.parseDouble(request.getParameter("price"));
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

	}

}
