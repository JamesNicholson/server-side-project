package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.WhiskeyDAO;
import model.Whiskey;

/**
 * Servlet implementation class CreateWhiskeyServlet
 */
@WebServlet("/CreateWhiskeyServlet")
public class CreateWhiskeyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateWhiskeyServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String brand = request.getParameter("brand");
		String product = request.getParameter("product");
		String year = request.getParameter("year");
		String description = request.getParameter("description");
		double price = Double.parseDouble(request.getParameter("price"));

		Whiskey whiskey = new Whiskey(brand, product, year, description, price);

		WhiskeyDAO.INSTANCE.save(whiskey);

		request.setAttribute("whiskey", whiskey);
		request.getRequestDispatcher("showWhiskey.jsp").forward(request, response);
	}

}
