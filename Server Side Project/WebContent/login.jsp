<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon" href="images/favicon.ico" />
<title>The Cooper's Stash</title>
</head>
<head>

<meta charset="utf-8" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/half-slider.css" rel="stylesheet">
</head>

<body>

	<!-- Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.jsp">The Cooper's Stash</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="login.jsp">Login</a></li>
				<li><a href="register.jsp">Register</a></li>
				<li><a href="LogoutServlet">Logout</a></li>
				<li><a><c:if test="${sessionScope.user != null }">
							<span class="whiteText">Current User: <c:out
									value="${sessionScope.user.userName }" /></span>
						</c:if></a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container --> </nav>

	<!-- Half Page Image Background Carousel Header -->
	<header id="myCarousel" class="carousel slide"> <!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
	</ol>

	<!-- Wrapper for Slides -->
	<div class="carousel-inner">
		<div class="item active">
			<!-- Set the first background image using inline CSS below. -->
			<div class="fill"
				style="background-image: url('images/header_barrels1.jpg');"></div>
			<div class="carousel-caption">
				<h2>The Home of Whiskey</h2>
			</div>
		</div>
		<div class="item">
			<!-- Set the second background image using inline CSS below. -->
			<div class="fill"
				style="background-image: url('images/header_barrels2.jpg');"></div>
			<div class="carousel-caption">
				<h2>Knowledge from our industry professionals</h2>
			</div>
		</div>
		<div class="item">
			<!-- Set the third background image using inline CSS below. -->
			<div class="fill"
				style="background-image: url('images/header_barrels3.jpg');"></div>
			<div class="carousel-caption">
				<h2>The Cooper's Secret Stash, Coming soon</h2>
			</div>
		</div>
	</div>

	<!-- Controls --> <a class="left carousel-control" href="#myCarousel"
		data-slide="prev"> <span class="icon-prev"></span>
	</a> <a class="right carousel-control" href="#myCarousel" data-slide="next">
		<span class="icon-next"></span>
	</a> </header>

	<!-- Page Content -->

	<div class="container">
		<c:if test="${sessionScope.user != null }">
			<h2>Current user logged in:</h2>
			<table>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Address Line 1</th>
					<th>Address Line 2</th>
					<th>Town</th>
					<th>County</th>
					<th>Username</th>
				</tr>
				<tr>
					<td>${sessionScope.user.firstName }</td>
					<td>${sessionScope.user.lastName }</td>
					<td>${sessionScope.user.addressLine1 }</td>
					<td>${sessionScope.user.addressLine2 }</td>
					<td>${sessionScope.user.town }</td>
					<td>${sessionScope.user.county }</td>
					<td>${sessionScope.user.userName }</td>
			</table>
		</c:if>

		<h2>Login</h2>
		<div class="row">
			<div class="col-lg-12 text-center">
				<form method="post" id="loginForm" action="LoginServlet">
					<label>Username</label> <input type="text" name="userName">
					<br /> <label>Password</label> <input type="password"
						name="password"></input> <br /> <input type="submit"
						value="Login">
				</form>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<hr>
				<p>Copyright &copy; The Cooper's Stash 2016</p>
			</div>
		</div>
	</div>
	</footer>
	<!-- /.row -->

	<!-- /.container -->

	<!-- jQuery Version 1.11.1 -->
	<script src="js/jquery.js" type="text/javascript"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js" type="text/javascript"></script>

	<!-- Script to Activate the Carousel -->
	<script>
		$('.carousel').carousel({
			interval : 5000
		//changes the speed
		})
	</script>

</body>
</html>