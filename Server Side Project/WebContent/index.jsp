<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon" href="images/favicon.ico" />
<title>The Cooper's Stash</title>
</head>
<head>

<meta charset="utf-8" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/half-slider.css" rel="stylesheet">
</head>

<body>

	<!-- Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.jsp">The Cooper's Stash</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="login.jsp">Login</a></li>
				<li><a href="register.jsp">Register</a></li>
				<li><a href="LogoutServlet">Logout</a></li>
				<li><a><c:if test="${sessionScope.user != null }"><span class = "whiteText">Current User: <c:out
							value="${sessionScope.user.userName }" /></span>
					</c:if></a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container --> </nav>

	<!-- Half Page Image Background Carousel Header -->
	<header id="myCarousel" class="carousel slide"> <!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
	</ol>

	<!-- Wrapper for Slides -->
	<div class="carousel-inner">
		<div class="item active">
			<!-- Set the first background image using inline CSS below. -->
			<div class="fill"
				style="background-image: url('images/header_barrels1.jpg');"></div>
			<div class="carousel-caption">
				<h2>The Home of Whiskey</h2>
			</div>
		</div>
		<div class="item">
			<!-- Set the second background image using inline CSS below. -->
			<div class="fill"
				style="background-image: url('images/header_barrels2.jpg');"></div>
			<div class="carousel-caption">
				<h2>Knowledge from our industry professionals</h2>
			</div>
		</div>
		<div class="item">
			<!-- Set the third background image using inline CSS below. -->
			<div class="fill"
				style="background-image: url('images/header_barrels3.jpg');"></div>
			<div class="carousel-caption">
				<h2>The Cooper's Secret Stash, Coming soon</h2>
			</div>
		</div>
	</div>

	<!-- Controls --> <a class="left carousel-control" href="#myCarousel"
		data-slide="prev"> <span class="icon-prev"></span>
	</a> <a class="right carousel-control" href="#myCarousel" data-slide="next">
		<span class="icon-next"></span>
	</a> </header>

	<!-- Page Content -->
	<!-- 	search whiskey container -->

	<h2>Search by brand</h2>
	<div class="container">
		<div class="col-lg-12 text-center">
			<form method="post" id="searchWhiskeyForm"
				action="SearchWhiskeyServlet" class="form-group row">
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Search </label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="searchBrand">
					</div>
				</div>
				<div class="form-group row">
					<div class="offset-sm-2 col-sm-10">
						<input type="button" id="searchWhiskeyButton" type="submit"
							value="Search" class="btn btn-primary"></input>
					</div>
				</div>
			</form>
		</div>
	</div>
	<hr>
	<!-- 	add whiskey container -->
	<div class="container">


		<h2>Add a whiskey to the database</h2>
		<div class="col-lg-12 text-center">
			<form method="post" id="whiskeyForm" action="CreateWhiskeyServlet">
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Brand</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="brand">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Product</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="product">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Year</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="year">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Description</label>
					<div class="col-sm-10">
						<textarea class="form-control" type="text" name="description"></textarea>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Price</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="price">
					</div>
				</div>
				<div class="form-group row">
					<div class="offset-sm-2 col-sm-10">
						<button id="addWhiskeyButton" type="submit" value="add"
							class="btn btn-primary" >Add</button>
					</div>
				</div>
			</form>
		</div>

<!-- <!-- 		<script> --> -->
<!-- // 			function successAlertFunction() { -->
<!-- // 				alert("Whiskey was added successfully!"); -->
<!-- // 			} -->
<!-- <!-- 		</script> --> -->
<!-- <!-- onclick="successAlertFunction()" --> -->
	</div>

	<!-- Footer -->
	<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<hr>
				<p>Copyright &copy; The Cooper's Stash 2016</p>
			</div>
		</div>
	</div>
	</footer>
	<!-- /.row -->

	<!-- /.container -->

	<!-- jQuery Version 1.11.1 -->
	<script src="js/jquery.js" type="text/javascript"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js" type="text/javascript"></script>

	<!-- Script to Activate the Carousel -->
	<script>
		$('.carousel').carousel({
			interval : 5000
		//changes the speed
		})
	</script>

</body>
</html>



